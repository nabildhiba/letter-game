﻿using System.Xml.Serialization;

namespace GamesHelpingModel
{
    public class WordCategoryNode
    {
        [XmlElement]
        public int Category { get; set; }

        [XmlElement("RootLetterNode")]
        public LetterNode RootLetterNode { get; set; }
        

        public WordCategoryNode()
        {

        }

        public WordCategoryNode(int ctg, LetterNode rlnode)
        {
            this.Category = ctg;
            this.RootLetterNode = rlnode;
        }
    }
}

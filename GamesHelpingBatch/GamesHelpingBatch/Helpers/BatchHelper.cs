﻿using GamesHelpingBatch.Services.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace GamesHelpingBatch.Helpers
{
    public static class BatchHelper
    {

        public static IGameHelpingService InstanciateService(string srv)
        {
            string nameClass = "GamesHelpingBatch.Services.Implementation." + srv + ", GamesHelpingBatch";
            Type objectType = Type.GetType(nameClass);
            IGameHelpingService GHS = (IGameHelpingService)Activator.CreateInstance(objectType);
            return GHS;
        }

        public static Dictionary<string, string> ArgumentMapper(string[] args)
        {
            if(args.Length <= 1)
            {
                throw new Exception("Not enough arguments");
            }
            
            var dicoArgs = new Dictionary<string, string>();
            var opt = String.Empty;
            var optValue = String.Empty;
            var isOptSet = false;

            foreach (var arg in args)
            {
                if (!isOptSet && arg.StartsWith("-"))
                {
                    opt = arg.Substring(1);
                    isOptSet = true;
                }
                else if (isOptSet)
                {
                    optValue = arg;
                    dicoArgs.Add(opt, optValue);
                    isOptSet = false;
                }
                else
                {
                    throw new Exception("Issue during the parsing of the arguments : " + arg);
                }
            }


            return dicoArgs;
        }

    }
}

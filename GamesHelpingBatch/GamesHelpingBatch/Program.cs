﻿using GamesHelpingBatch.Helpers;
using GamesHelpingBatch.Services.Interface;
using System;

namespace GamesHelpingBatch
{
    class Program
    {
        static void Main(string[] args)
        {
            var dicoArgs = BatchHelper.ArgumentMapper(args);

            if ((dicoArgs == null) || (!dicoArgs.ContainsKey("process")))
            {
                throw new Exception("No process specified in the arguments of the command line");
            }

            IGameHelpingService GHS = BatchHelper.InstanciateService(dicoArgs["process"]);
            var retCode = GHS.Run(dicoArgs);

            Console.WriteLine("Execution returned with code : " + (int)retCode);

        }
    }
}

﻿using GamesHelpingBatch.Entities;
using GamesHelpingBatch.Services.Interface;
using GamesHelpingModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace GamesHelpingBatch.Services.Implementation
{
    public class WordTreeFrequencyService : IGameHelpingService
    {

        #region Interface Implementation
        public RefRetCodes Run(Dictionary<string, string> dicoArgs)
        {

            //First we validate that all the arguments are well set
            var retCode = ValidateArguments(dicoArgs);

            if (retCode != RefRetCodes.OK)
            {
                return retCode;
            }

            //We take the path of the dico
            string pathToDico = dicoArgs["dicopath"];
            string pathToLetterValue = dicoArgs["dicolv"];

            if (!File.Exists(pathToDico))
            {
                throw new Exception("Dico file does not exist : " + pathToDico);
            }

            if (!File.Exists(pathToLetterValue))
            {
                throw new Exception("LetteValue file does not exist : " + pathToLetterValue);
            }

            //First we need to get the LetterValue and transform is into a usable object by our code
            Dictionary<string, int> LetterValue = ParseLetterValueDico(pathToLetterValue);

            //This is the root that will contain all the word categories
            //A word category is equal to its length
            WordTreeFrequencyNode wtfreqRootNode = new WordTreeFrequencyNode();

            //This is a dico of all the word categories to accelerate the researches
            Dictionary<int, WordCategoryNode> dicoWCNodes = new Dictionary<int, WordCategoryNode>();

            try
            {
                using (StreamReader sr = new StreamReader(pathToDico))
                {
                    string line;

                    while ((line = sr.ReadLine()) != null)
                    {
                        if (String.IsNullOrWhiteSpace(line))
                        {
                            //We don't need empty lines
                            continue;
                        }

                        WordCategoryNode currWCNode;


                        if (!dicoWCNodes.ContainsKey(line.Length))
                        {
                            //If the category doesn't exist we just need to create it and add it to the 
                            // - Word Frequency rootnode
                            // - WordCategory dico
                            currWCNode = new WordCategoryNode(line.Length, new LetterNode(String.Empty, 0, 0, null));
                            dicoWCNodes.Add(line.Length, currWCNode);
                            wtfreqRootNode.WCNodes.Add(currWCNode);
                        }
                        else
                        {
                            //If the category already exists we just need to retrieve it to alter its letternode.
                            currWCNode = dicoWCNodes[line.Length];
                        }


                        this.ProcessWordTree(currWCNode.RootLetterNode, LetterValue, line);

                    }

                    this.GenerateWordTreeFrequencyXML(wtfreqRootNode, Path.GetDirectoryName(pathToDico));

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



            return RefRetCodes.OK;
        }

        public RefRetCodes ValidateArguments(Dictionary<string, string> dicoArgs)
        {
            if (!dicoArgs.ContainsKey("dicopath"))
            {
                return RefRetCodes.MissingMandatoryArguments;
            }

            if (String.IsNullOrEmpty(dicoArgs["dicopath"]))
            {
                return RefRetCodes.MandatoryArgumentNotSet;
            }

            if (!dicoArgs.ContainsKey("dicolv"))
            {
                return RefRetCodes.MissingMandatoryArguments;
            }

            if (String.IsNullOrEmpty(dicoArgs["dicolv"]))
            {
                return RefRetCodes.MandatoryArgumentNotSet;
            }

            if (dicoArgs.Count != 3)
            {
                return RefRetCodes.WrongNumberOfArguments;
            }

            return RefRetCodes.OK;
        }
        #endregion

        #region Private Methods

        private void GenerateWordTreeFrequencyXML(WordTreeFrequencyNode wtfreqRootNode, string path)
        {
            string xmlPath = Path.Combine(path, "WTFRXML.xml");
            XmlSerializer xs = new XmlSerializer(typeof(WordTreeFrequencyNode));
            TextWriter tw = new StreamWriter(xmlPath);
            xs.Serialize(tw, wtfreqRootNode);
        }

        private void ProcessWordTree(LetterNode rootLetterNode, Dictionary<string, int> letterValue , string line)
        {
            LetterNode currLN = rootLetterNode;
            int lineValue = this.CalculateWordValue(line, letterValue);

            foreach (char c in line)
            {
                currLN.CountGenerableWords++;
                currLN.MaxGenerableScore = Math.Max(currLN.MaxGenerableScore, lineValue);

                if (currLN.ChildrenLetters == null)
                {
                    currLN.ChildrenLetters = new List<LetterNode>();
                }

                LetterNode nextLN = null;

                if (!currLN.ChildrenLetters.Any(x => x.Letter == c.ToString()))
                {
                    nextLN = new LetterNode(c.ToString(), 0, 0, null);
                    currLN.ChildrenLetters.Add(nextLN);
                }
                else
                {
                    nextLN = currLN.ChildrenLetters.Where(x => x.Letter == c.ToString()).FirstOrDefault();
                }

                currLN = nextLN;

            }

            currLN.CountGenerableWords++;
            currLN.MaxGenerableScore = Math.Max(currLN.MaxGenerableScore, lineValue);
        }

        private Dictionary<string, int> ParseLetterValueDico(string path)
        {
            Dictionary<string, int> resLetterValue = new Dictionary<string, int>();
            if (File.Exists(path))
            {


                var lines = File.ReadAllLines(path);

                foreach (var line in lines)
                {
                    var values = line.Split(';');

                    resLetterValue.Add(values[0], int.Parse(values[1]));
                }
            }

            return resLetterValue;

        }

        private int CalculateWordValue(string word, Dictionary<string, int> letterValue)
        {
            var retVal = 0;
            
            if (letterValue.Count > 0)
            {
                foreach (char c in word)
                {
                    if (letterValue.ContainsKey(c.ToString()))
                    {
                        retVal += letterValue[c.ToString()];
                    }
                }
            }
            return retVal;
        }


        #endregion
    }
}

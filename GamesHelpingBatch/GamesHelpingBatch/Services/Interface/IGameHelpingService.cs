﻿using GamesHelpingBatch.Entities;
using System.Collections.Generic;

namespace GamesHelpingBatch.Services.Interface
{
    public interface IGameHelpingService
    {
        RefRetCodes ValidateArguments(Dictionary<string, string> dicoArgs);

        RefRetCodes Run(Dictionary<string, string> dicoArgs);

    }
}

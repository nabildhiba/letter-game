﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GamesHelpingBatch.Entities
{
    /*
     * This is the list of all retcodes that all the methodes can return
     * 0 will always be the OK return code
     * !=0 will be a specific return code that needs to be explained
    */
    public enum RefRetCodes
    {
        OK = 0,

        //KO Statuses
        MissingMandatoryArguments = -1,
        MandatoryArgumentNotSet = -2,
        WrongNumberOfArguments = -3
    }
}

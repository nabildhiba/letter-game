﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFallingLetterMovementScript : MonoBehaviour
{
    [Header("LetterMovement")]
    private Rigidbody2D letterRigibody;
    public float letterFallingSpeed;


    private void Start()
    {
        letterRigibody = GetComponent<Rigidbody2D>();
        letterRigibody.velocity = new Vector2(0f, letterFallingSpeed);
    }

}

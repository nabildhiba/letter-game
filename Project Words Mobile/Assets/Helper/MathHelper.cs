﻿using System;
using Random = UnityEngine.Random;

namespace Assets.Helper
{
    public static class MathHelper
    {

        public static int RandomPickingBetweenValues(params int[] values)
        {
            var val = Random.Range(0, values.Length - 1);
            return values[val];
        }
    }
}

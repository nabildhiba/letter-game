﻿using Assets.Scripts;
using Assets.Scripts.Services.Implementation;
using Assets.Scripts.Services.Interface;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Helper
{
    public class PrefabHelper : Singleton<PrefabHelper>
    {
        public enum BonusPrefab
        {
            NoBonus = 0,
            GoodLetter = 1,
            BadLetter = 2,
            GoldenLetter = 3
        }

        public List<GameObject> listBonusesPrefab;
        public List<Sprite> listLettersSprite;
        private object loadXmlFileRequest;

        void Awake()
        {
            destroyOnLoad = false;
            base.Awake();
        }

        //public int WordCategory {
        //    get
        //    {
        //        return this.WordCategory;
        //    }
            
        //    set
        //    {
        //        if(value >= 2 && value <= 22)
        //        {
        //            this.WordCategory = value;
        //        }
        //        else
        //        {
        //            this.WordCategory = 2;
        //        }
        //    }

        //}


        public GameObject GetBonusPrefab(BonusPrefab bonusPrefab)
        {
            return listBonusesPrefab[(int)bonusPrefab];
        }

        public Sprite GetLetterSprite(String keycodestr)
        {
            return listLettersSprite[(int)(keycodestr[0] - 'A')];
        }

        public LetterSuggestorWithDifficultyCurveService loadedNextLetterSuggestor;

        public IEnumerator LoadLetterSuggestor()
        {
            //It is quick but I kept it like this load asynchronosly is more fluid.
            ResourceRequest loadXmlFileRequest = Resources.LoadAsync<TextAsset>("WTFRXML");
            while (!loadXmlFileRequest.isDone)
            {
                yield return loadXmlFileRequest;
            }
            if (LetterSuggestorNeedToBeLoaded())
            {
                TextAsset xmlFile = (TextAsset)loadXmlFileRequest.asset;
                //The main reason why we have freezes
                string text = xmlFile.text;
                Thread thread = new Thread(
                    () => { InstantiateLetterSuggestor(30, 30, text); }
                    );
                thread.Start();
                while (thread.IsAlive)
                {
                    yield return null;
                }
            }
            SceneManager.LoadSceneAsync("MainScene", LoadSceneMode.Single);
        }

        private bool LetterSuggestorNeedToBeLoaded()
        {
            return loadedNextLetterSuggestor == null;
        }

        private void InstantiateLetterSuggestor(int a, int b, string xmlString)
        {
            loadedNextLetterSuggestor = new LetterSuggestorWithDifficultyCurveService(a, b, xmlString);
            Debug.Log("Dictionary loaded");
        }

        public INextLettersSuggestorService GetBenchmarkLetterSuggestor(float pctGL)
        {
            var xmlFile = Resources.Load<TextAsset>("WTFRXML");

            var bnch = new LetterSuggestorBenchmark(pctGL, xmlFile.text);

            return bnch;
        }

        public IWordValidatorService GetWordValidator()
        {
            var txtFile = Resources.Load<TextAsset>("WordDico");

            var wrdDico = new TextWordValidatorService(txtFile.text);

            return wrdDico;
        }

        public IScoreCalculatorService GetScoreCalculator()
        {
            var txtFile = Resources.Load<TextAsset>("LetterValue");

            var scrCalc = new ScoreCalculatorWithBoostMultiplierService(txtFile.text);

            return scrCalc;
        }

        public WordZoneService GetWordZone()
        {
            Debug.Log("WordCategory = " + PlayerPrefs.GetInt("WordCategory"));

            return new WordZoneService(PlayerPrefs.GetInt("WordCategory"));
        }
    }
}

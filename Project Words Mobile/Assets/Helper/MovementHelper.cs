﻿using Assets.Scripts.MovementScripts.Implementation;
using Assets.Scripts.MovementScripts.Interface;
using UnityEngine;

namespace Assets.Helper
{
    public static class MovementHelper
    {
        public static void LookAt2D(this Transform transform, Transform targetTransform)
        {
            Vector3 dir = targetTransform.position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        public static ITrajectory GetRandomTrajectory()
        {
            return new SinusTrajectory();
        }

    }
}

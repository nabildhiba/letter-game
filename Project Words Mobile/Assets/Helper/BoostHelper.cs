﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Helper
{
    public static class BoostHelper
    {
        private static Boost NOBOOST = new Boost("", 1);
        private static Boost BOOSTX2 = new Boost("BiFlow", 2);
        private static Boost BOOSTX4 = new Boost("TetraStyle", 4);
        private static Boost BOOSTX8 = new Boost("OctaBoost", 8);
        private static Boost BOOSTX16 = new Boost("HexaDecaBoost", 16);
        private static Boost BOOSTX32 = new Boost("DotriaContaBoost", 32);

        public readonly static List<Boost> availableBoost = new List<Boost> { NOBOOST, BOOSTX2, BOOSTX4, BOOSTX8, BOOSTX16, BOOSTX32 };
        public static void InitiateBoost(ref Boost boost) => boost = NOBOOST;

        public static Boost GetBoost(int boostMultiplicator)
        {
            return availableBoost.Single(j => j.BoostMultiplicator == boostMultiplicator);
        }

        public static void LowerBoost(ref Boost boost)
        {
            if (boost == NOBOOST)
            {
                return;
            }
            boost = GetBoost(boost.BoostMultiplicator / 2);
        }

        public static void IncreaseBoost(ref Boost boost)
        {
            if (boost == BOOSTX32)
            {
                return;
            }
            boost = GetBoost(boost.BoostMultiplicator * 2);
        }
    }
}

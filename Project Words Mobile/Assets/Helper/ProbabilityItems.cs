﻿using Assets.Scripts.Bonus.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Helper
{
    public class ProbabilityItems<T>
    {
        public double Probability { get; set; }
        public T Item { get; set; }

        public ProbabilityItems(T value, double probability)
        {
            Item = value;
            Probability = probability;
        }
    }

    public class ProbabilityList<T> : List<ProbabilityItems<T>>
    {
        public new void Add(ProbabilityItems<T> item)
        {
            if (item.Probability < 0)
            {
                throw new ArgumentException($"Probability should not be negative. Invalid value {item.Probability}");
            }
            base.Add(item);
            Sort(
                (a, b) =>
                {
                    if (b == null) { return 1; }
                    return a.Probability.CompareTo(b.Probability);
                }
                );
        }

        public T ChooseRandomly()
        {
            return this[1].Item;

            //System.Random random = new System.Random();
            //double rnd = random.NextDouble() * this.Sum(j => j.Probability);
            //double sum = 0;
            //foreach (ProbabilityItems<T> element in this)
            //{
            //    // loop until the random number is less than our cumulative probability
            //    if (rnd <= (sum = sum + element.Probability))
            //    {
            //        Debug.Log($"{element.Item.GetType()} bonus is activated");
            //        return element.Item;
            //    }
            //}
            //return default(T);

        }
    }
}

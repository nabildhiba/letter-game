﻿using Assets.Scripts;
using System.Collections.Generic;

namespace Assets.Helper
{
    public static class CorridorHelper
    {
        public static int CorridorDefaultFallingSpeed = 4;

        public static void MultiplyFallingSpeedAllCorridors(this IEnumerable<Corridor> corridors, float fallingSpeedAllCorridors)
        {
            foreach (var corridor in corridors)
            {
                corridor.FallingSpeedRatio = fallingSpeedAllCorridors;
            }
        }
    }
}

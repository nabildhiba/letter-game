﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Helper
{
    public static class EnumerableHelper
    {

        public static KeyValuePair<TKey, TValue> GetRandomKeyValuePair<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            List<TKey> values = Enumerable.ToList(dict.Keys);
            int size = dict.Count;
            Random rand = new Random();
            TKey randomKey = values[rand.Next(size)];
            return dict.First(j => j.Key.Equals(randomKey));
        }
    }
}

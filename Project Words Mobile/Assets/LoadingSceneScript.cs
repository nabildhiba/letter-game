﻿using Assets.Helper;
using Assets.Scripts.Services.Interface;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingSceneScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PrefabHelper.Instance.LoadLetterSuggestor());
    }
}

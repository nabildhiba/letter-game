﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameMenuScript : MonoBehaviour
{
    public static bool FlushLetterIsClicked;

    public void ReturnToGame()
    {
        Time.timeScale = 1;
    }

    public void ReturnToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }

    public void FlushLettersOnClick()
    {
        FlushLetterIsClicked = true;
        GameManager.Instance.wordZone.FlushLetters();
        Debug.Log("Flushing");
    }
}

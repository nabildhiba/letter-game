﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Services.Implementation 
{ 
    public class WordHistoryService
    {
        #region Properties

        private List<string> _wordHisto;
        private int _nbWordMax;

        #endregion

        #region Constructor

        public WordHistoryService(int nbMax)
        {
            this._wordHisto = new List<string>();
            this._nbWordMax = nbMax;
        }

        #endregion

        #region Methodes

        public int AddWordToHistory(string word)
        {
            if (String.IsNullOrEmpty(word))
            {
                return (int)RefRetCodes.WH_WordEmptyOrNull;
            }

            if (this._wordHisto.Count >= this._nbWordMax)
            {
                //We need to remove the oldest value
                this._wordHisto.RemoveAt(0);

            }

            //The most recent word is added at the 
            this._wordHisto.Add(word);     

            return (int)RefRetCodes.OK;
        }

        public string GetHisto()
        {
            var retStr = string.Empty;

            for(int i = this._wordHisto.Count-1; i >= 0; i--)
            {
                retStr += this._wordHisto[i];

                if (i != 0)
                {
                    retStr += "\n";
                }
            }

            return retStr;
        }

        #endregion

    }
}

﻿using Assets.Scripts.Entities;
using Assets.Scripts.Services.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Assets.Helper;

namespace Assets.Scripts.Services.Implementation
{
    public class WordTreeLetterSuggestorService : INextLettersSuggestorService
    {
        #region Properties
        protected WordTreeFrequencyNode WordTreeFreqNode { get; set; }
        protected const string ENGLISH_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // The letters in Dico are upper so the const must be upper otherwise we have toUpperInvariant everywhere
        #endregion


        #region Constructor
        public WordTreeLetterSuggestorService(string xmlWTF)
        {
            if (!String.IsNullOrEmpty(xmlWTF))
            {
                this.FillWordTreeFrequency(xmlWTF);
            }
        }


        #endregion

        #region Methods
        protected void FillWordTreeFrequency(string xmlWTF)
        {
            //Here we are just going to deserialize the XML where the Word Tree Frequency was generated
            XmlSerializer xs = new XmlSerializer(typeof(WordTreeFrequencyNode));

            using (StringReader reader = new StringReader(xmlWTF))
            {
                this.WordTreeFreqNode = (WordTreeFrequencyNode)xs.Deserialize(reader);
            }


        }


        protected void GetGoodAndBadLetterSuggestionsList(int cat, string word, out Dictionary<string, LetterNode> goodLettersDico, out Dictionary<string, LetterNode> badLettersDico)
        {
            if (this.WordTreeFreqNode == null)
            {
                throw new Exception("The Word Tree Frequence is not initiated");
            }

            if (word.Length >= cat)
            {
                throw new Exception("The Word's length is equal or bigger than the wanted category");
            }

            goodLettersDico = null;
            badLettersDico = null;
            //We search for the category of the word
            var curentWordCatNode = this.WordTreeFreqNode.WCNodes.Where(x => x.Category == cat).FirstOrDefault();

            if (curentWordCatNode == null)
            {
                throw new Exception("Category of word " + cat + " not handled");
            }

            //Now we have the rootletter of the category which should be equal to String.Empty
            var currentLetterNode = curentWordCatNode.RootLetterNode;

            foreach (var c in word)
            {
                //We search if for the given letter exists in the currentLetterNode
                currentLetterNode = currentLetterNode.ChildrenLetters != null ?
                                        currentLetterNode.ChildrenLetters.Where(x => x.Letter == c.ToString()).FirstOrDefault() :
                                        null
                                        ;

                if (currentLetterNode == null)
                {
                    //if the letter doesn't exist in the currentLetterNode childre
                    //This means the word doesn't exist in the dictionnary used to generate the XML
                    //So we stop here and return null
                    badLettersDico = CreateDictionaryFromStringList(ENGLISH_ALPHABET.ToListString());
                    return;
                }

                //else we continue until we attain last letter's node

            }
            //Then we transform its children into a dictionnary where the letter is the key and the value is its frequency of apparition
            goodLettersDico = currentLetterNode.ChildrenLetters.ToDictionary(x => x.Letter, y => y);

            List<string> letters = currentLetterNode.ChildrenLetters.Select(j => j.Letter).ToList();
            IEnumerable<string> badLetters = ENGLISH_ALPHABET.ToListString().Where(j => !letters.Contains(j));
            badLettersDico = CreateDictionaryFromStringList(badLetters);
        }

        protected Dictionary<string, LetterNode> CreateDictionaryFromStringList(IEnumerable<string> letters)
        {
            if (!letters.Any())
            {
                return null;
            }
            Dictionary<string, LetterNode> dictionary = new Dictionary<string, LetterNode>();
            foreach (var letter in letters)
            {
                dictionary.Add(letter, new LetterNode(letter, 1, 0, null));
            }
            return dictionary;
        }


        #endregion

        #region Implementation

        public virtual char SuggestNextLetter(int cat, string word, out bool isGoodLetter)
        {
            Dictionary<string, LetterNode> goodLettersDico;
            Dictionary<string, LetterNode> badLettersDico;
            GetGoodAndBadLetterSuggestionsList(cat, word, out goodLettersDico, out badLettersDico);
            isGoodLetter = true;
            return goodLettersDico.GetRandomKeyValuePair().Key[0];//We return a random good letter ==> this class can be used for testing
        }

        #endregion
    }
}

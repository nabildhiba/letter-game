﻿using Assets.Scripts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Services.Implementation
{
    public class WordZoneService
    {

        #region properties
        private int _nbMaxLetters = 3;
        private string _word = string.Empty;

        public int NbMaxLetters
        {
            get
            {
                return _nbMaxLetters;
            }
        }

        public string Word
        {
            get
            {
                return _word;
            }
            set
            {
                _word = value;
                GuiManager.WordingText = value;
            }
        }

        #endregion

        #region Constructor
        public WordZoneService(int nbML)
        {
            this._nbMaxLetters = nbML;
        }
        #endregion

        #region Methods

        public int AddLetter(string l)
        {
            if (string.IsNullOrEmpty(l))
            {
                return (int)RefRetCodes.WZ_LetterEmptyOrNull;
            }

            var strWrd = this.Word;

            strWrd += l;

            if (strWrd.Length > this._nbMaxLetters)
            {
                return (int)RefRetCodes.WZ_NbLetterMaxPassed;
            }
            else
            {

                this.Word = strWrd;

                if (this.Word.Length == this._nbMaxLetters)
                {
                    return (int)RefRetCodes.WZ_nbLettersEqualMax;
                }

                return (int)RefRetCodes.OK;
            }
        }

        public void FlushLetters()
        {
            this.Word = string.Empty;
        }

        #endregion


    }
}

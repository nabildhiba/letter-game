﻿using Assets.Helper;
using Assets.Scripts.Entities;
using System;
using UnityEngine;
using static Assets.Helper.BoostHelper;
using static Assets.Helper.SequenceHelper;

namespace Assets.Scripts.Services.Implementation
{
    public class ScoreCalculatorWithBoostMultiplierService : ScoreCalculatorService
    {

        private DateTime? lastTimeWordIsValid;

        public Boost ActiveBoost;

        public ScoreCalculatorWithBoostMultiplierService(string ltrVal): base(ltrVal)
        {
            InitiateBoost(ref ActiveBoost);
            Debug.Log($"ActiveBoost is {ActiveBoost.BoostMultiplicator} ");
        }

        public override void OnUpdateFrame()
        {
            if (IfBoostExpired())
            {
                LowerBoost(ref ActiveBoost);
                Debug.Log($"ActiveBoost is {ActiveBoost.BoostMultiplicator} ");

                lastTimeWordIsValid = DateTime.Now;
            }
        }

        public override void OnUpdateFrame(object sender, EventArgs e)
        {

        }

        public override void NotifyWrongWordIsCompleted()
        {
            InitiateBoost(ref ActiveBoost); // If wrong word we reinitiate all boost
            Debug.Log($"ActiveBoost is {ActiveBoost.BoostMultiplicator} ");

            lastTimeWordIsValid = null;
        }

        public override int CalculateWordValue(string word)
        {
            UpdateBoostAfterWordCompletion();

            var retVal = 0;

            if (this._letterValue.Count > 0)
            {
                foreach (char c in word)
                {
                    retVal += this._letterValue[c.ToString()];
                }
            }
            return retVal * ActiveBoost.BoostMultiplicator;
        }

        public override Boost GetBoost()
        {
            return ActiveBoost;
        }

        private void UpdateBoostAfterWordCompletion()
        {
            if (CurrentSuccessiveCompletedWordCountForBoost() % 2 == 0)
            {
                //HigherBoost
                IncreaseBoost(ref ActiveBoost);
                Debug.Log($"ActiveBoost is {ActiveBoost.BoostMultiplicator} ");

            }
            lastTimeWordIsValid = DateTime.Now;
        }

        private bool IfBoostExpired()
        {
            if (DateTime.Now == null)
            {
                return false;
            }
            return DateTime.Now - lastTimeWordIsValid > ActiveBoost.BoostLifeSpan;
        }
    }
}

﻿using Assets.Scripts.Services.Interface;
using System;

namespace Assets.Scripts.Services.Implementation
{
    public class TestWordValidatorService : IWordValidatorService
    {

        #region Properties
        private int K = 0;
        #endregion

        #region Constructor

        public TestWordValidatorService() { }

        #endregion

        #region Methods

        public Boolean ValidateWord(string word)
        {
            //For now we are alterning true and false for test purposes
            //return true;
            return (K++ % 2) == 0;
        }

        #endregion

    }
}

﻿using Assets.Scripts.Services.Interface;
using System.Collections.Generic;
using System.IO;

namespace Assets.Scripts.Services.Implementation
{
    public class TextWordValidatorService : IWordValidatorService
    {

        #region Properties

        private HashSet<string> _wordDico = null;

        #endregion

        #region Constructor

        public TextWordValidatorService(string wrdDico)
        {
            this.FillDicoWord(wrdDico);
        }
        #endregion


        #region Methods
        public bool ValidateWord(string word)
        {
            return this._wordDico.Contains(word);
        }

        private void FillDicoWord(string wrdDico)
        {

            if (string.IsNullOrEmpty(wrdDico))
            {
                return;
            }

            var lines = wrdDico.Split(new string[] { "\r\n" }, System.StringSplitOptions.None);
            this._wordDico = new HashSet<string>();

            foreach(var line in lines)
            {
                this._wordDico.Add(line);
            }


        }

        #endregion
    }
}

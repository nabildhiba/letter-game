﻿using Assets.Helper;
using Assets.Scripts.Entities;
using Assets.Scripts.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Services.Implementation
{


    public class LetterSuggestorBenchmark : WordTreeLetterSuggestorService
    {

        #region Properties

        public float PercentageOfGoodLetters { get; set; }

        private List<char> suggestedLettersHistory { get; set; }
        #endregion

        #region Constructor

        public LetterSuggestorBenchmark(float pctGL, string xmlWTF) : base(xmlWTF)
        {
            this.PercentageOfGoodLetters = pctGL;
            suggestedLettersHistory = new List<char>();
        }

        #endregion


        #region Implementation
        public override char SuggestNextLetter(int cat, string word, out bool isGoodLetter)
        {
            Dictionary<string, LetterNode> goodLettersDico;
            Dictionary<string, LetterNode> badLettersDico;
            GetGoodAndBadLetterSuggestionsList(cat, word, out goodLettersDico, out badLettersDico);

            KeyValuePair<string, LetterNode> firstHigherLetter;
            if (this.PercentageOfGoodLetters > 100)
            {
                firstHigherLetter = goodLettersDico.OrderByDescending(i => i.Value.MaxGenerableScore).First();
                isGoodLetter = true;
            }
            else
            {
                float goodNextLetterProbability = this.PercentageOfGoodLetters / 100;
                float random = UnityEngine.Random.Range(0f, 1f);
                if (random <= goodNextLetterProbability)
                {
                    firstHigherLetter = goodLettersDico != null ? goodLettersDico.GetRandomKeyValuePair() : badLettersDico.GetRandomKeyValuePair();
                    isGoodLetter = true;
                }
                else
                {
                    firstHigherLetter = badLettersDico != null ? badLettersDico.OrderBy(j => LastIndexAppartition(char.Parse(j.Key))).First() : goodLettersDico.GetRandomKeyValuePair();
                    isGoodLetter = false;
                }
            }
            suggestedLettersHistory.Add(firstHigherLetter.Key[0]);
            return firstHigherLetter.Key[0];
        }


        private int LastIndexAppartition(char letter)
        {
            return suggestedLettersHistory.LastIndexOf(letter);
        }
        #endregion
    }
}

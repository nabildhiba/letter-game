﻿using Assets.Scripts.Entities;
using Assets.Scripts.Services.Interface;
using System;
using System.Collections.Generic;
using System.IO;

namespace Assets.Scripts.Services.Implementation
{
    public class ScoreCalculatorService : IScoreCalculatorService
    {

        #region Properties
        protected Dictionary<string, int> _letterValue = new Dictionary<string, int>();

        #endregion

        #region Constructor

        public ScoreCalculatorService(string ltrVal)
        {
            this.ParseLetterValueDico(ltrVal);
        }
        #endregion

        #region Methods

        private void ParseLetterValueDico(string ltrVal)
        {
            if (String.IsNullOrEmpty(ltrVal))
            {
                return;
            }

            var lines = ltrVal.Split(new string[] { "\r\n" }, System.StringSplitOptions.None);


            foreach (var line in lines)
            {
                var values = line.Split(';');

                this._letterValue.Add(values[0], int.Parse(values[1]));
            }

        }

        public virtual int CalculateWordValue(string word)
        {
            var retVal = 0;

            if (this._letterValue.Count > 0)
            {
                foreach (char c in word)
                {
                    if (this._letterValue.ContainsKey(c.ToString()))
                    {
                        retVal += this._letterValue[c.ToString()];
                    }
                }
            }
            return retVal;
        }

        public virtual void NotifyWrongWordIsCompleted()
        {
            throw new NotImplementedException();
        }

        public virtual void OnUpdateFrame()
        {
            throw new NotImplementedException();
        }

        public virtual Boost GetBoost()
        {
            throw new NotImplementedException();
        }

        public virtual void OnUpdateFrame(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}

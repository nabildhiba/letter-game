﻿using Assets.Helper;
using Assets.Scripts.Entities;
using Assets.Scripts.Services.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Assets.Scripts.Services.Implementation
{
    public class LetterSuggestorWithDifficultyCurveService : WordTreeLetterSuggestorService
    {

        #region properties

        //like suggested in the name ==> this property is to handle the chances to drop a bad letter ==> chances go up as the difficulty increases
        private float _chancesDropBadLetter;

        //like suggested in the name ==> this property is to handle the chances to drop the letter with the best KPI ==> chances go down as the difficulty increases
        //the KPI is : "CountGenerableWords x MaxGenerableScore
        private float _chancesDropBestLetter;

        public float ChancesDropBadLetter
        {
            get
            {
                return this._chancesDropBadLetter;
            }

            set
            {
                if (value >= 0 && value <= 100)
                {
                    this._chancesDropBadLetter = value;
                }
            }
        }


        public float ChancesDropBestLetter
        {
            get
            {
                return this._chancesDropBestLetter;
            }

            set
            {
                if (value >= 0 && value <= 100)
                {
                    this._chancesDropBestLetter = value;
                }
            }
        }

        private List<char> suggestedLettersHistory { get; set; }

        #endregion


        #region Constructor

        public LetterSuggestorWithDifficultyCurveService(float chancesDropBadLetter = 30, float chancesDropBestLetter = 30, string xmlWTF = null) : base(xmlWTF)
        {
            //For now we put fix chances until we variabilise this
            this._chancesDropBadLetter = chancesDropBadLetter;
            this._chancesDropBestLetter = chancesDropBestLetter;
            suggestedLettersHistory = new List<char>();
        }


        #endregion

        #region Implementation
        public override char SuggestNextLetter(int cat, string word, out bool isGoodLetter)
        {
            //First we get all the good and bad letters that we can use
            Dictionary<string, LetterNode> goodLettersDico;
            Dictionary<string, LetterNode> badLettersDico;
            GetGoodAndBadLetterSuggestionsList(cat, word, out goodLettersDico, out badLettersDico);

            //The ramdom of UnityEngine is exclusive for the max number that's why we need to put 101 instead of 100
            int random = UnityEngine.Random.Range(0, 101);

            //We check if we need to throw a bad letter first
            if (badLettersDico != null && (random <= this._chancesDropBadLetter || goodLettersDico == null))
            {
                isGoodLetter = false;
                var badlettersOrder = badLettersDico.OrderBy(j => LastIndexAppartition(char.Parse(j.Key))).ToList();
                char badLetterSuggested = badlettersOrder.First().Key[0];
                suggestedLettersHistory.Add(badLetterSuggested);
                return badLetterSuggested;
            }

            if (goodLettersDico.Count == 1)
            {
                isGoodLetter = true;
                return goodLettersDico.First().Key[0];
            }


            random = UnityEngine.Random.Range(0, 101);
            //We retrieve the best letter that is the one with the highest KPI ==> CountGenerableWords * MaxGenerableScore
            char bestLetter = goodLettersDico.OrderBy(x => x.Value.CountGenerableWords * x.Value.MaxGenerableScore).Last().Key[0];


            //We check if we need to throw the best letter with random generated
            //or the best letter is the only letter there is
            if (random <= this._chancesDropBestLetter)
            {
                isGoodLetter = true;
                return bestLetter;
            }

            //We remove the best letter from the possible letters to give
            goodLettersDico.Remove(bestLetter.ToString());

            isGoodLetter = true;
            return goodLettersDico.GetRandomKeyValuePair().Key[0];

        }

        private int LastIndexAppartition(char letter)
        {
            return suggestedLettersHistory.LastIndexOf(letter);
        }

        #endregion
    }
}

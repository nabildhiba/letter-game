﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Services.Interface
{
    public interface IWordValidatorService
    {
        Boolean ValidateWord(string word);
    }
}

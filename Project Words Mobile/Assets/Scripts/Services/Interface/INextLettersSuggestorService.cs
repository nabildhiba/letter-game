﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Services.Interface
{
    public interface INextLettersSuggestorService
    {

        char SuggestNextLetter(int cat, string word, out bool isGoodLetter);


    }
}

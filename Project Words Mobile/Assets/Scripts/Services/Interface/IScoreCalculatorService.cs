﻿
using Assets.Scripts.Entities;
using System;

namespace Assets.Scripts.Services.Interface
{
    public interface IScoreCalculatorService
    {
        void OnUpdateFrame();
        void OnUpdateFrame(object sender, EventArgs e);

        Boost GetBoost();

        void NotifyWrongWordIsCompleted();

        int CalculateWordValue(string word);
    }
}

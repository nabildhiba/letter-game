﻿using Assets.Scripts;
using UnityEngine;

public class FallingLetterScript : MonoBehaviour
{
    public KeyCode correspondantKey;
    public int slot;
    // This is the gaining score the button has. We didn't specified it yet, but it will allow me to have a refactor code for
    // DestroyFallingLetterAndRespawnANewOne 
    public int scoreOfTheLetter;

    public FallingLetterScript()
    {
        scoreOfTheLetter = 1;//defaultValue
    }

    protected virtual void DestroyFallingLetterAndRespawnANewOne(bool fallingLetterWonByPlayer)
    {
        Destroy(this);
        Destroy(gameObject);
        if (GameManager.IsBenchmarking)
        {
            fallingLetterWonByPlayer = true;
        }
        GameManager.Instance.HandleOnDestroy(fallingLetterWonByPlayer ? correspondantKey.ToString() : "", slot, fallingLetterWonByPlayer ? scoreOfTheLetter : 0);
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

namespace Assets.Scripts.PrefabScripts
{
    public class FallingStandardLetterScript : FallingLetterScript
    {

        EventSystem _eventSystem;

        private float pressButtonTimer;
        private float pressButtonDelay = 0.02f;

        private void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                pressButtonTimer = Time.time + pressButtonDelay;
            }
        }

        private void Start()
        {
            _eventSystem = EventSystem.current;
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.tag == "BorderScreen")
            {
                DestroyFallingLetterAndRespawnANewOne(false);
            }
        }

        private void OnTriggerStay2D(Collider2D col)
        {
            if (col.gameObject.tag == "LetterReceptor")
            {
                if (pressButtonTimer > Time.time)/* && GetObjectClickedOn() != "FlushButton")*/ // We verify that we are not in fact clicking on a game button
                {

                    if (!IsPointOverUIElement("FlushButton"))
                    {
                        Debug.Log("Destroying");
                        DestroyFallingLetterAndRespawnANewOne(true);
                    }
                }
            }
        }

        private bool IsPointOverUIElement(string name)
        {
            var eventData = new PointerEventData(_eventSystem);
            eventData.position = Input.mousePosition;
            var results = new List<RaycastResult>();
            _eventSystem.RaycastAll(eventData, results);
            foreach (var item in results)
            {
                if (name.Contains(item.gameObject.name))
                {
                    return true;
                }
            }
            return false;
        }
    }
}

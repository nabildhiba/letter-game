﻿using System;
using UnityEngine;

namespace Assets.Scripts.PrefabScripts
{
    /// <summary>
    /// Maybe I should have created a FallingScriptStandard but maybe also everyScript should be independant
    /// </summary>
    public class FallingGoldenLetterScript : FallingLetterScript
    {
        public FallingGoldenLetterScript()
        {
            scoreOfTheLetter = 1;//defaultValue
        }



        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.tag == "BorderScreen")
            {
                throw new InvalidOperationException("The golden letter should be intercepted by the LetterReceptor");
            }
        }

        private void OnTriggerStay2D(Collider2D col)
        {
            if (col.gameObject.tag == "InsideLetterReceptor")
            {
                DestroyFallingLetterAndRespawnANewOne(true);
            }
        }
    }
}

﻿namespace Assets.Scripts
{

    public class Corridor
    {
        public Corridor(float positionX, float fallingSpeedRatio)
        {
            this.floatingLetterspositionX = positionX;
            this.FallingSpeedRatio = fallingSpeedRatio;
        }

        public float floatingLetterspositionX;
        public float FloatingLettersPositionY = 2.51f;
        public int scorePerCorridor;
        public float FallingSpeedRatio;
        public int ScorePerCorridor
        {
            get { return scorePerCorridor; }
            set
            {
                scorePerCorridor = value;
                //Insert here: game logic when score is increased per corridor;
            }
        }
    }
}

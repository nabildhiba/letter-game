﻿using Assets.Helper;
using Assets.Scripts.MovementScripts.Interface;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.MovementScripts.Implementation
{
    public class LinearTrajectory : ITrajectory
    {
        public Vector2 speed = new Vector2(2, 2);

        public Vector2 direction;

        public void CalibrateToLookRightAs(Transform transform)
        {
            direction = transform.right;
        }

        public Vector2 GetInitialSpawnPosition(Vector2 center, float radius)
        {
            var x = Random.Range(-radius, radius);

            var y = (float)Math.Sqrt(Math.Pow(radius, 2f) - Math.Pow(x, 2f)) * (float)MathHelper.RandomPickingBetweenValues(-1, 1);

            var retRes = new Vector2(x, y);

            return retRes;
        }

        public Vector2 GetMovementVector()
        {
            return new Vector2(
                speed.x * direction.x,
                speed.x * direction.y
                );
        }
    }
}

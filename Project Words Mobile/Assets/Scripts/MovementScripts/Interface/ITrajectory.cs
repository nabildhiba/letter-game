﻿using UnityEngine;

namespace Assets.Scripts.MovementScripts.Interface
{
    public interface ITrajectory
    {

        Vector2 GetInitialSpawnPosition(Vector2 center, float radius); //Gets the intersection between the trajectory and the radius
        Vector2 GetMovementVector(); // Get tangent vector for trajectory
        void CalibrateToLookRightAs(Transform transform);

    }
}

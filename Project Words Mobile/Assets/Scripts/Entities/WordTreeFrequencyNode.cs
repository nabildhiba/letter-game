﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Assets.Scripts.Entities
{
    public class WordTreeFrequencyNode
    {
        [XmlArray("WordCategories")]
        public List<WordCategoryNode> WCNodes { get; set; }

        public WordTreeFrequencyNode()
        {
            this.WCNodes = new List<WordCategoryNode>();
        }
    }
}

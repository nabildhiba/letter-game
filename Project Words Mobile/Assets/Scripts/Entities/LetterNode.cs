﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Assets.Scripts.Entities
{
    public class LetterNode
    {
        [XmlElement]
        public string Letter { get; set; }

        [XmlElement]
        public int CountGenerableWords { get; set; }

        [XmlElement]
        public int MaxGenerableScore { get; set; }

        [XmlArray]
        public List<LetterNode> ChildrenLetters { get; set; }


        public LetterNode()
        {
            this.Letter = "";
            this.CountGenerableWords = 0;
            this.MaxGenerableScore = 0;
            this.ChildrenLetters = null;
        }

        public LetterNode(string c, int freq, int maxGenScore, List<LetterNode> chld)
        {
            this.Letter = c;
            this.CountGenerableWords = freq;
            this.MaxGenerableScore = maxGenScore;
            this.ChildrenLetters = chld;
        }

    }

}

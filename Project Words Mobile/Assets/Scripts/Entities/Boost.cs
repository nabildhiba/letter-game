﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Entities
{
    public class Boost
    {


        int _boostMultiplicator;


        public int BoostMultiplicator { get { return _boostMultiplicator; } }


        /// <summary>
        /// IUPAC Multiplicator 
        /// </summary>
        string _boostDescription;

        public string BoostDescription { get { return _boostDescription; } }


        private TimeSpan _boostLifeSpan;
        public TimeSpan BoostLifeSpan { get { return _boostLifeSpan; } }

        public Boost(string boostDescription, int boostMultiplicator, TimeSpan? lifeSpan = null)
        {
            _boostDescription = boostDescription;
            _boostMultiplicator = boostMultiplicator;
            _boostLifeSpan = lifeSpan ?? TimeSpan.FromSeconds(10);
        }
    }
}

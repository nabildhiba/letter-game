﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Entities
{

    /*
     * This is the list of all retcodes that all the methodes can return
     * 0 will always be the OK return code
     * !=0 will be a specific return code that needs to be explained
    */
    public enum RefRetCodes
    {
        OK = 0,
        //Other OK returnCodes
        WZ_nbLettersEqualMax = 1,
        //Errors
        WZ_NbLetterMaxPassed = -1,
        WZ_LetterEmptyOrNull = -2,
        WH_WordEmptyOrNull = -3,
        NLS_WordCategoryNotHandled = -4


    }


}

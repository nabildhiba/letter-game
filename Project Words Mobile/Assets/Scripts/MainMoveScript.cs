﻿using UnityEngine;
// Function called about 60 times per second
public class MainMoveScript : MonoBehaviour
{
    public float maxSpeed = 20f;

     void FixedUpdate()
    {
        Debug.Log("FixedUpdate time :" + Time.deltaTime);
    }
     void Update()
    {
        // Get the rigidbody component
        Rigidbody2D r2d = GetComponent<Rigidbody2D>();
        var move = Input.GetAxis("Horizontal");
        r2d.velocity = new Vector2(tresholdMove(move) * maxSpeed  , r2d.velocity.y);
    }

    /// <summary>
    /// Tresholds the move.
    /// </summary>
    /// <returns>The move.</returns>
    /// <param name="move">Move.</param>
    private float tresholdMove(float move)
    {
        if (move <= -0.5)
        {
            move = -0.5f;
        }
        else if (move > -0.5f && move <= -0.1f)
        {
            move = -0.2f;
        }
        else if (move > -0.1f && move <= 0.1f)
        {
            move = 0f;
        }
        else if (move > 0.1f && move <= 0.5f)
        {
            move = 0.2f;
        }
        else
        {
            move = 0.5f;
        }
        return move;
    }
}
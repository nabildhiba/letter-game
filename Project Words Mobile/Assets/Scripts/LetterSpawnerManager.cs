﻿using Assets.Helper;
using UnityEngine;

namespace Assets.Scripts
{
    public class LetterSpawnerManager : MonoBehaviour
    {
        public Corridor[] Corridors
        {
            get { return GameManager.Instance.Corridors; }
        }

        public GameObject Prefab
        {
            get { return GameManager.Instance.prefabFallingLetters; }
        }

        public GameObject LetterWithSpritePrefab
        {
            get { return GameManager.Instance.prefabFallingLettersWithSprite; }
        }


        public void SpawnRandomFallingLetterFromLetterFrequency(char nextLetter, bool isGoodLetter)
        {
            System.Random slotRandom = new System.Random();
            //Today we have one corridor
            int nbCorridor = slotRandom.Next(Corridors.Length);

            KeyCode nextKeyCode = this.convertCharToKeyCode(nextLetter);
            SpawnFallingLetterFromSlot(nextKeyCode, nbCorridor);
        }


        public GameObject SpawnFallingLetterFromSlot(KeyCode keycode, int slot)
        {
            return SpawnFallingLetter(keycode, Corridors[slot], slot, $"FallingLetter{slot}");
        }

        public void HandlOneDestroy(int slot, char nextLetter)
        {
            KeyCode nextKeyCode = this.convertCharToKeyCode(nextLetter);
            SpawnFallingLetterFromSlot(nextKeyCode, slot);
        }

        private GameObject SpawnFallingLetter(KeyCode keycode, Corridor corridor, int slot, string name)
        {
            //GameObject fallingletter = (GameObject)Instantiate(Prefab, new Vector3(corridor.floatingLetterspositionX, corridor.FloatingLettersPositionY), Quaternion.identity);
            //FallingLetterScript fallingLetterScript = fallingletter.GetComponent<FallingLetterScript>();
            //fallingLetterScript.correspondantKey = keycode;
            //fallingLetterScript.slot = slot;
            //fallingletter.name = name;
            //fallingletter.GetComponent<TextMesh>().text = keycode.ToString();
            //var rigidbody = fallingletter.GetComponent<Rigidbody2D>();
            //rigidbody.velocity = new Vector2(rigidbody.velocity.x, -corridor.FallingSpeedRatio);


            GameObject fallingletterwithsprite = (GameObject)Instantiate(LetterWithSpritePrefab, new Vector3(corridor.floatingLetterspositionX /*- 2.5f*/, corridor.FloatingLettersPositionY), Quaternion.identity);
            FallingLetterScript fallingLetterScriptWithSprite = fallingletterwithsprite.GetComponent<FallingLetterScript>();
            fallingLetterScriptWithSprite.correspondantKey = keycode;
            fallingLetterScriptWithSprite.slot = slot;
            fallingletterwithsprite.name = name;
            fallingletterwithsprite.GetComponent<SpriteRenderer>().sprite = PrefabHelper.Instance.GetLetterSprite(keycode.ToString());
            var rigidbodyWithSprite = fallingletterwithsprite.GetComponent<Rigidbody2D>();
            rigidbodyWithSprite.velocity = new Vector2(rigidbodyWithSprite.velocity.x, -corridor.FallingSpeedRatio);
//#if DEBUG
//            fallingletter.GetComponent<TextMesh>().color = GameManager.Instance.isGoodLetter ? Color.green : Color.red;
//#endif
            return fallingletterwithsprite;
        }

        private KeyCode convertCharToKeyCode(char c)
        {
            int kcode = c - 65;
            kcode += 97;
            return (KeyCode)kcode;
        }
    }
}

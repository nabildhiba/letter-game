﻿using Assets.Scripts.Bonus.Interface;
using static Assets.Helper.SequenceHelper;

using System;

namespace Assets.Scripts.Bonus.Implementation
{
    public class TimeEtensionBonus : IBonus
    {

        public GameManager GameManagerSingleton
        {
            get
            {
                return GameManager.Instance;
            }
        }

        public void OnActivation()
        {
            GameManagerSingleton.targetTime += 20;
            ResetSuccessiveCompletedWordCountForBonus();
        }

        public void OnDestroyLetterEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnTimeScaleStoppedEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnUpdateEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnWordFinishedEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void SubscribeEvents()
        {
        }

        public void UnsubscribeEvents()
        {
            throw new NotImplementedException();
        }
    }
}

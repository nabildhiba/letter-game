﻿using Assets.Scripts.Bonus.Interface;
using Assets.Scripts.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Assets.Helper.SequenceHelper;


namespace Assets.Scripts.Bonus.Implementation
{
    public class WheelOfLettersBonus : IBonus
    {

        public GameManager GameManagerSingleton
        {
            get
            {
                return GameManager.Instance;
            }
        }

        private string wordBeforeBonusActivation;
        private float previousChancesDropBadLetter;
        private int wordCat;
        private int nbOfClicks = 0;

        public void OnActivation()
        {
            this.GameManagerSingleton.StopBonusTime();
            this.wordBeforeBonusActivation = this.GameManagerSingleton.wordZone.Word;
            this.nbOfClicks = 60;

            var letterSuggestorService = GameManagerSingleton.nextLetterSuggestor as LetterSuggestorWithDifficultyCurveService;
            previousChancesDropBadLetter = letterSuggestorService.ChancesDropBadLetter;
            letterSuggestorService.ChancesDropBadLetter = 0;

            wordCat = GameManagerSingleton.wordZone.NbMaxLetters;

        }


        public void OnUpdateEvent(object sender, EventArgs e)
        {
            if (nbOfClicks <= 0)
            {
                ResetSuccessiveCompletedWordCountForBonus();
                GameManagerSingleton.activeBonus = null;
                this.nbOfClicks = 0;

                var letterSuggestorService = GameManagerSingleton.nextLetterSuggestor as LetterSuggestorWithDifficultyCurveService;
                letterSuggestorService.ChancesDropBadLetter = previousChancesDropBadLetter;

                var isGoodLetter = false;
                var nxtL = GameManagerSingleton.nextLetterSuggestor.SuggestNextLetter(wordCat, GameManagerSingleton.wordZone.Word, out isGoodLetter);
                GameManagerSingleton.letterSpawnerManager.SpawnRandomFallingLetterFromLetterFrequency(nxtL, isGoodLetter);

                this.wordCat = 0;

                UnsubscribeEvents();
                this.GameManagerSingleton.ResumeBonusTime();

            }
        }

        public void OnTimeScaleStoppedEvent(object sender, EventArgs e)
        {
            nbOfClicks--;
            if (nbOfClicks % 4 == 0)
            {
                bool isGoodLetter = false;
                var nextWord = this.wordBeforeBonusActivation + this.GameManagerSingleton.nextLetterSuggestor.SuggestNextLetter(wordCat, wordBeforeBonusActivation, out isGoodLetter);

                GameManagerSingleton.wordZone.FlushLetters();
                GameManagerSingleton.wordZone.AddLetter(nextWord);
            }
        }

        public void SubscribeEvents()
        {
            GameManagerSingleton.OnUpdateEventHandler += OnUpdateEvent;
            GameManagerSingleton.OnTimeScaleStoppedEventHandler += OnTimeScaleStoppedEvent;
        }

        public void UnsubscribeEvents()
        {
            GameManagerSingleton.OnUpdateEventHandler -= OnUpdateEvent;
            GameManagerSingleton.OnTimeScaleStoppedEventHandler -= OnTimeScaleStoppedEvent;
        }

        public void OnDestroyLetterEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnWordFinishedEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}

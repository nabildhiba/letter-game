﻿
using Assets.Scripts.Bonus.Interface;
using System;

namespace Assets.Scripts.Bonus.Implementation
{
    public class TimedBonus : IBonus
    {
        protected float? TimedBonusStartTime;
        protected float TimedBonusDuration = 10;

        public GameManager GameManagerSingleton
        {
            get
            {
                return GameManager.Instance;
            }
        }

        public virtual void OnActivation()
        {
            TimedBonusStartTime = GameManagerSingleton.targetTime;
        }

        public virtual void OnDestroyLetterEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public virtual void OnTimeScaleStoppedEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public virtual void OnUpdateEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public virtual void OnWordFinishedEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public virtual void SubscribeEvents()
        {
            throw new NotImplementedException();
        }

        public virtual void UnsubscribeEvents()
        {
            throw new NotImplementedException();
        }
    }
}

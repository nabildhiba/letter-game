﻿using System;
using UnityEngine;
using static Assets.Helper.SequenceHelper;
using static Assets.Helper.PrefabHelper;
using Assets.Helper;

namespace Assets.Scripts.Bonus.Implementation
{
    public class GlassesBonus : TimedBonus
    {
        public override void OnUpdateEvent(object sender, EventArgs e)
        {
            if (TimedBonusStartTime - GameManagerSingleton.targetTime > TimedBonusDuration)
            {
                GameManagerSingleton.prefabFallingLettersWithSprite = PrefabHelper.Instance.GetBonusPrefab(BonusPrefab.NoBonus);
                ResetSuccessiveCompletedWordCountForBonus();
                TimedBonusStartTime = null;
                GameManagerSingleton.activeBonus = null;
                UnsubscribeEvents();
            }
        }

        public override void OnDestroyLetterEvent(object sender, EventArgs e)
        {
            SetPrefabAccordingToLetter();
        }

        public override void OnWordFinishedEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }


        private void SetPrefabAccordingToLetter()
        {

            if (GameManagerSingleton.isGoodLetter)
            {
                GameManagerSingleton.prefabFallingLettersWithSprite = PrefabHelper.Instance.GetBonusPrefab(BonusPrefab.GoodLetter);
            }
            else
            {
                GameManagerSingleton.prefabFallingLettersWithSprite = PrefabHelper.Instance.GetBonusPrefab(BonusPrefab.BadLetter);
            }
        }

        public override void SubscribeEvents()
        {
            GameManagerSingleton.OnUpdateEventHandler += OnUpdateEvent;
            GameManagerSingleton.OnDestroyLetterEventHandler += OnDestroyLetterEvent;
        }

        public override void UnsubscribeEvents()
        {
            GameManagerSingleton.OnUpdateEventHandler -= OnUpdateEvent;
            GameManagerSingleton.OnDestroyLetterEventHandler -= OnDestroyLetterEvent;
        }
    }
}

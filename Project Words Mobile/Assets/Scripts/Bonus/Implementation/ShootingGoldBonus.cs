﻿using Assets.Helper;
using Assets.Scripts.Services.Implementation;
using System;
using static Assets.Helper.SequenceHelper;
using static Assets.Helper.CorridorHelper;
using Assets.Scripts.Bonus.Interface;
using UnityEngine;
using static Assets.Helper.PrefabHelper;

namespace Assets.Scripts.Bonus.Implementation
{
    public class ShootingGoldBonus : IBonus
    {

        const int SHOOTINGGOLDWORDSTOFINISH = 2;
        private int finishedWordsWithThisBonus = 0;

        public GameManager GameManagerSingleton
        {
            get
            {
                return GameManager.Instance;
            }
        }


        float previousChancesDropBadLetter;

        //Todo here: create a GoldenLetterSuggestor!!
        public void OnActivation()
        {
            //GameManagerSingleton.prefabFallingLetters = PrefabHelper.Instance.GetBonusPrefab(BonusPrefab.GoldenLetter);
            GameManagerSingleton.prefabFallingLettersWithSprite = PrefabHelper.Instance.GetBonusPrefab(BonusPrefab.GoldenLetter);

            // We assume that  the lettersuggestor is the one with difficultyCurveService;

            var letterSuggestorService = GameManagerSingleton.nextLetterSuggestor as LetterSuggestorWithDifficultyCurveService;
            previousChancesDropBadLetter = letterSuggestorService.ChancesDropBadLetter;

            letterSuggestorService.ChancesDropBadLetter = 0;
            GameManagerSingleton.Corridors.MultiplyFallingSpeedAllCorridors(4);
        }

        public void OnDestroyLetterEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void OnWordFinishedEvent(object sender, EventArgs e)
        {
            finishedWordsWithThisBonus++;
            if (finishedWordsWithThisBonus == SHOOTINGGOLDWORDSTOFINISH)
            {
                //GameManagerSingleton.prefabFallingLetters = PrefabHelper.Instance.GetBonusPrefab(BonusPrefab.NoBonus); 
                GameManagerSingleton.prefabFallingLettersWithSprite = PrefabHelper.Instance.GetBonusPrefab(BonusPrefab.NoBonus);
                ResetSuccessiveCompletedWordCountForBonus();
                (GameManagerSingleton.nextLetterSuggestor as LetterSuggestorWithDifficultyCurveService).ChancesDropBadLetter = previousChancesDropBadLetter;
                GameManagerSingleton.Corridors.MultiplyFallingSpeedAllCorridors(CorridorDefaultFallingSpeed);
                finishedWordsWithThisBonus = 0;
                UnsubscribeEvents();
                GameManagerSingleton.activeBonus = null;
            }
        }

        public void OnUpdateEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void UnsubscribeEvents()
        {
            GameManagerSingleton.OnWordFinishedEventHandler -= OnWordFinishedEvent;
        }

        public void SubscribeEvents()
        {
            GameManagerSingleton.OnWordFinishedEventHandler += OnWordFinishedEvent;
        }

        public void OnTimeScaleStoppedEvent(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}

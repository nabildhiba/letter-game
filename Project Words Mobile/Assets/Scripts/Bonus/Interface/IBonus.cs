﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Bonus.Interface
{
    public interface IBonus
    {
        void OnActivation();
        void OnUpdateEvent(object sender, EventArgs e);
        void OnDestroyLetterEvent(object sender, EventArgs e);
        void OnWordFinishedEvent(object sender, EventArgs e);
        void OnTimeScaleStoppedEvent(object sender, EventArgs e);

        void SubscribeEvents();
        void UnsubscribeEvents();
    }
}

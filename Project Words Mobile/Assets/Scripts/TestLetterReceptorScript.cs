﻿using Assets.Scripts.MovementScripts.Implementation;
using Assets.Scripts.MovementScripts.Interface;
using UnityEngine;
using static Assets.Helper.MovementHelper;

public class TestLetterReceptorScript : MonoBehaviour
{
    public Vector2 spawningPosition;

    public GameObject movingLetterPrefab;

    private Rigidbody2D movingLetterRigidBody;

    public double cooldown = 3f;

    public ITrajectory traj;

    public float radius;

    private bool canSpawn
    {
        get
        {
            return (spawningPosition != null && cooldown <= 0f);
        }
    }

    // Use this for initialization
    void Start()
    {
        traj = GetRandomTrajectory(); //MOVE TO GAME SCRIPT
        radius = 5f;
        spawningPosition = traj.GetInitialSpawnPosition(transform.position, radius); //MOVE TO GAME SCRIPT
        CreateLetter();
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
        }

        if (movingLetterRigidBody != null)
        {
            movingLetterRigidBody.velocity = traj.GetMovementVector();
        }
    }


    void CreateLetter()
    {
        if (canSpawn)
        {
            var mvLetter = Instantiate(movingLetterPrefab) as GameObject;
            movingLetterRigidBody = mvLetter.GetComponent<Rigidbody2D>();
            mvLetter.transform.position = new Vector3(spawningPosition.x, spawningPosition.y, 0);
            mvLetter.transform.LookAt2D(transform);
            traj.CalibrateToLookRightAs(mvLetter.transform);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        spawningPosition = traj.GetInitialSpawnPosition(transform.position, radius);
        CreateLetter();
    }
}

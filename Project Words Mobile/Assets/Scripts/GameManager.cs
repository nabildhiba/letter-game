﻿using Assets.Scripts;
using Assets.Scripts.Entities;
using Assets.Scripts.Services.Implementation;
using Assets.Scripts.Services.Interface;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static Assets.Helper.SequenceHelper;
using static Assets.Helper.CorridorHelper;
using Assets.Scripts.Bonus.Interface;
using Assets.Scripts.Bonus.Implementation;
using Assets.Helper;

public class GameManager : Singleton<GameManager>
{
    public GameObject pausePanel;
    public GameObject gameOverPanel;
    public GameObject gameOverText; // Is a child of gameOverPanel. I use drag and drop here because it is faster thant gameOver.Panel.GetChild("gameOverText");
    public int FallingSpeed;

    public Animator fadeScreenCanvasPrefabAnimator;


    public Boost ActiveBoost;

    public IBonus activeBonus;

    public ProbabilityList<IBonus> listAvailableBonus = new ProbabilityList<IBonus>() {
        new ProbabilityItems<IBonus>(new GlassesBonus(),50),
        new ProbabilityItems<IBonus>(new WheelOfLettersBonus(),30),
        new ProbabilityItems<IBonus>(new ShootingGoldBonus(),15),
        new ProbabilityItems<IBonus>(new TimeEtensionBonus(),5)
    };


    #region Events
    internal EventHandler OnUpdateEventHandler;
    internal EventHandler OnDestroyLetterEventHandler;
    internal EventHandler OnWordFinishedEventHandler;
    internal EventHandler OnTimeScaleStoppedEventHandler;
    #endregion

    //TimerZone : Value in seconds
    public float targetTime;
    public int wordCategory;
    private bool isGameOver = false;
    private bool startOftheGame;

    private int currentWordScore;
    internal bool isGoodLetter;
    private bool isBonusGamePaused = false;

    #region Services
    internal WordZoneService wordZone;
    private IWordValidatorService wordValidator;
    private IScoreCalculatorService scoreService;
    private WordHistoryService wordHistory;
    internal INextLettersSuggestorService nextLetterSuggestor;
    internal LetterSpawnerManager letterSpawnerManager;
    #endregion

    #region Benchmark
    public static bool IsBenchmarking;

    /// <summary>
    /// For benchmark use only: from 0 to 100: probability(/100) of showing a good nextLetter. If >100 = we generate always the best letter for maximum word score
    /// </summary>
    public static float percentageOfGoodLetters;

    public DateTime startTime;
    public string csvToWrite;
    #endregion


    #region Prefab
    public GameObject prefabFallingLetters;
    public GameObject prefabFallingLettersWithSprite;
    #endregion

    public Corridor[] Corridors = new Corridor[] { new Corridor(0, CorridorDefaultFallingSpeed) };

    public TimeSpan timeSpanBetweenFallingLetters = new TimeSpan(0, 0, 0, 0, 5000);
    //, -1, 0.5f, 2 };


    //todo: Maybe we no longer need GeneralScriptSingleton now.
    protected override void Awake()
    {
        base.Awake();
        InitiateBenchmarkIfAvailable();
        this.targetTime = 100f;
        Time.timeScale = 1;
        wordCategory = PlayerPrefs.GetInt("WordCategory");
        InitializeServices();
    }

    // Use this for initialization
    void Start()
    {
        OnUpdateEventHandler += scoreService.OnUpdateFrame;
        startOftheGame = true;
    }

    private void InitializeServices()
    {
        wordZone = PrefabHelper.Instance.GetWordZone();
        wordValidator = PrefabHelper.Instance.GetWordValidator();
        scoreService = PrefabHelper.Instance.GetScoreCalculator();
        wordHistory = new WordHistoryService(5);
        letterSpawnerManager = new LetterSpawnerManager();
        this.nextLetterSuggestor = PrefabHelper.Instance.loadedNextLetterSuggestor;
        ResetSuccessiveCompletedWordCountAll();
    }

    private void Update()
    {
        if (IsGameNotStartedYet())
        {
            return;
        }
        //We want this to run at the start of the "Game" once only
        if (startOftheGame)
        {
            char dicoNextLetters = this.nextLetterSuggestor.SuggestNextLetter(wordCategory, this.wordZone.Word, out isGoodLetter);
            letterSpawnerManager.SpawnRandomFallingLetterFromLetterFrequency(dicoNextLetters, isGoodLetter);
            startOftheGame = false;
        }
        if (isGameOver)
        {
            return;
        }
        targetTime -= Time.deltaTime;
        //Debug.Log($"Update started on the game scene: {targetTime}");

        if (isBonusGamePaused)
        {
            OnTimeScaleStoppedEventHandler?.Invoke(null, EventArgs.Empty);
        }

        OnUpdateEventHandler?.Invoke(null, EventArgs.Empty);

        Boost newActiveBoost = scoreService.GetBoost();
        if (ActiveBoost != newActiveBoost)
        {
            ActiveBoost = newActiveBoost;
            GuiManager.UpdateBoostText(newActiveBoost.BoostDescription, newActiveBoost.BoostLifeSpan);
        }
        GuiManager.TimeText = this.GetFormattedRemainingTime();

        if (this.targetTime <= 0.0f)
        {
            if (activeBonus == null)
            {
                GameOver();
            }
        }

        if (!this.isGameOver && Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }

    private bool IsGameNotStartedYet()
    {

        //If the intro animation is not loaded yet
        if (fadeScreenCanvasPrefabAnimator != null)
        {
            return !(fadeScreenCanvasPrefabAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
        }
        return false;
    }

    public void HandleOnDestroy(string correspondantKey, int destroyedObjectSlot, int scoreIncreaseValue)
    {
        this.AddLetter(correspondantKey);
        var dicoNextLetters = this.nextLetterSuggestor.SuggestNextLetter(wordCategory, this.wordZone.Word, out isGoodLetter);

        OnDestroyLetterEventHandler?.Invoke(null, EventArgs.Empty);
        if (!isBonusGamePaused)
        {
            letterSpawnerManager.HandlOneDestroy(destroyedObjectSlot, dicoNextLetters);
        }
    }

    private void AddLetter(string letter)
    {
        int retCode = this.wordZone.AddLetter(letter);

        //If the adding letter worked perfectly
        if (retCode == (int)RefRetCodes.OK || retCode == (int)RefRetCodes.WZ_nbLettersEqualMax)
        {
            // Avoid call Wording GameObject unnecessary

            if (retCode == (int)RefRetCodes.WZ_nbLettersEqualMax)
            {
                this.ValidateWord();
            }
        }
    }

    private void ValidateWord()
    {
        if (this.wordValidator.ValidateWord(this.wordZone.Word))
        {
            IncreaseSuccessiveCompletedWordCount();
            currentWordScore = this.scoreService.CalculateWordValue(this.wordZone.Word);
            this.AddWordToHistory();
            this.IncreaseScore();
            string[] randomBonusCatchPhrases = { "Splendid!", "There you go!", "A man with such culture", "You rock!" };

            GuiManager.PlayBonusAnimation(randomBonusCatchPhrases[UnityEngine.Random.Range(0, randomBonusCatchPhrases.Length - 1)]);
        }
        else
        {
            ResetSuccessiveCompletedWordCountAll();
            int scoreToDecrease = 5;
            this.DecreaseScore(scoreToDecrease);
            string[] randomMalusCatchPhrases = { "Too bad!", "Can't make words!", "No such word", "Ouch!Not a word!" };
            string malusText = "- 5 points!";
            malusText += "\n" + randomMalusCatchPhrases[UnityEngine.Random.Range(0, randomMalusCatchPhrases.Length - 1)];
            GuiManager.PlayMalusAnimation(malusText);
        }
        //Reset currentWordScore for the next word to be calculated. 
        currentWordScore = 0;

        this.wordZone.FlushLetters();
        OnWordFinishedEventHandler?.Invoke(null, EventArgs.Empty);

        ManageBonus();
    }

    private void ManageBonus()
    {
        var lastGoodWordsCount = CurrentSuccessiveCompletedWordCountForBonus();
        if (lastGoodWordsCount >= 2)
        {
            if (activeBonus == null)
            {
                activeBonus = listAvailableBonus.ChooseRandomly();
                activeBonus.OnActivation();
                activeBonus.SubscribeEvents();
            }
        }
    }

    private void AddWordToHistory()
    {
        if (IsBenchmarking)
        {
            //Write 
            var secondLapse = DateTime.Now - startTime;
            csvToWrite += $"{wordZone.Word};{currentWordScore};{GuiManager.Score + currentWordScore};{secondLapse.TotalSeconds}" + "\n";
        }

        this.wordHistory.AddWordToHistory(this.wordZone.Word + "(" + currentWordScore + ")");
        GuiManager.WordingHistoryText = "Word Histo :\n" + this.wordHistory.GetHisto();
    }

    private void IncreaseScore()
    {
        //For now we will not increase the corridor s score as only the GuiManager Score is important
        GuiManager.Score += currentWordScore;
    }

    private void DecreaseScore(int valueToDecreaseBy)
    {
        GuiManager.Score = GuiManager.Score > valueToDecreaseBy ? GuiManager.Score - valueToDecreaseBy : 0;
    }



    private void PauseGame()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }


    public void StopBonusTime()
    {
        Time.timeScale = 0;
        isBonusGamePaused = true;
    }

    public void ResumeBonusTime()
    {
        Time.timeScale = 1;
        isBonusGamePaused = false;
    }

    private void GameOver()
    {
        Time.timeScale = 0;
        var rankOfGameScore = HighScoreManager.Instance.GetScoreRankInHighScore(GuiManager.Score);
        if (GuiManager.Score == 0)
        {
            gameOverText.GetComponent<Text>().text = $"Score zero! Too bad ! Try next time and prove your worth !";
        }
        else if (rankOfGameScore <= 10)
        {
            HighScoreManager.Instance.SaveHighScore(wordCategory.ToString(), GuiManager.Score);
            gameOverText.GetComponent<Text>().text = $"Bravo!  You have a new highscore : {GuiManager.Score}points. Your ranked {rankOfGameScore}! Thanks for playing!!!";
        }
        else
        {
            gameOverText.GetComponent<Text>().text = $"Good game, you scored {GuiManager.Score} points! Try next time for better score!!!!";
        }
        gameOverPanel.SetActive(true);
        this.isGameOver = true;
        if (IsBenchmarking)
        {
            string filename = $"{percentageOfGoodLetters}_{FallingSpeed}_{DateTime.Now:yyyyMMddHHmmss}.csv";
            File.WriteAllText(filename, csvToWrite);
            Time.timeScale = 0;
        }
    }

    private void InitiateBenchmarkIfAvailable()
    {
        var benchmarkFileTypePath = Path.Combine(Directory.GetCurrentDirectory(), @"Assets\ExtRessources\LaunchParamDebug.txt");
        if (File.Exists(benchmarkFileTypePath))
        {
            string[] comments = { "//", "'", "#" };
            var lines = File.ReadAllLines(benchmarkFileTypePath).Where(X => !comments.Any(Y => X.StartsWith(Y))).ToArray();
            if (lines.Length >= 3 && lines[0] == "1")
            {
                IsBenchmarking = true;
                csvToWrite = "";
                startTime = DateTime.Now;
                percentageOfGoodLetters = float.Parse(lines[1], CultureInfo.InvariantCulture.NumberFormat);
                FallingSpeed = int.Parse(lines[2]);
                CorridorDefaultFallingSpeed = FallingSpeed;
            }

            if (IsBenchmarking)
            {
                nextLetterSuggestor = PrefabHelper.Instance.GetBenchmarkLetterSuggestor(percentageOfGoodLetters);
            }

        }
    }

    private string GetFormattedRemainingTime()
    {
        var retStr = "00:00";
        if (this.targetTime >= 0.0f)
        {
            var minutes = Math.Floor(this.targetTime / 60.0f);
            var seconds = Math.Floor(this.targetTime - (minutes * 60.0f));

            retStr = minutes.ToString("00") + ":" + seconds.ToString("00");
        }
        return retStr;
    }
}
